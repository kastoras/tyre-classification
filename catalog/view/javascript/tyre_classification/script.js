$(document).ready(function () {
    $("#close-all-steel").click(function () {
        $(".all-steel-table-cat").hide();
        $("#close-all-steel").hide();
        $("#open-all-steel").show();
    });

    $("#open-all-steel").click(function () {
        $(".all-steel-table-cat").show();
        $("#close-all-steel").show();
        $("#open-all-steel").hide();
    });
});