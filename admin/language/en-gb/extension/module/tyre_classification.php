<?php
// Heading
$_['heading_title']    = 'Tyre Classification';
 
// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified Multi Mod module!';
$_['text_edit']        = 'Edit Multi Mod Module';
 
// Entry
$_['entry_name']       = 'Module Name';
$_['entry_type']       = 'Τύπος';
$_['entry_status']     = 'Status';

//Options
$_['default']       = 'Επιλογή';
$_['all_steel_classification']       = 'All Steel Classification Table';
$_['all_steel_classification_home']  = 'All Steel Classification Table Home';

 
// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Multi Mod module!';
$_['error_name']       = 'Module Name must be between 3 and 64 characters!';
$_['error_message']    = 'Message required!';
