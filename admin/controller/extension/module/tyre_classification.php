<?php 
class ControllerExtensionModuleTyreClassification extends Controller {

    private $error = array();
 
    public function index() {
		$this->load->language('extension/module/tyre_classification'); // load the language
 
		$this->document->setTitle($this->language->get('heading_title'));
 
		$this->load->model('setting/module'); // loads the model
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

			$currentModuleId = $this->request->get['module_id'];
			if (isset($this->request->get['module_id']) && $this->request->get['module_id']!="") {				
				$this->model_setting_module->editModule($this->request->get['module_id'], $this->request->post);// edits a pre-existing module				
			} else {
				$this->model_setting_module->addModule('tyre_classification', $this->request->post);// adds a new module
				$currentModuleId = $this->db->getLastId();
			}

			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('extension/module/tyre_classification', 
				'user_token=' . $this->session->data['user_token'].'&module_id='.$currentModuleId, 'SSL'));
		}

		$data['heading_title'] = $this->language->get('heading_title');
 
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
 
        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_type'] = $this->language->get('entry_type');
		$data['entry_status'] = $this->language->get('entry_status');
 
		$data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');
        
        $data['module_types'] = [
            ''=>$this->language->get('default'),
            'kama_all_steel_classification' => $this->language->get('all_steel_classification'),
            'kama_all_steel_classification_home' => $this->language->get('all_steel_classification_home'),
        ];
 
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
 
		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}
 
		if (isset($this->error['message'])) {
			$data['error_message'] = $this->error['message'];
		} else {
			$data['error_message'] = '';
		}
 
		$data['breadcrumbs'] = array();
 
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], 'SSL')
		);
 
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'user_token=' . $this->session->data['user_token'], 'SSL')
		);
 
		if (!isset($this->request->get['module_id'])) {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('extension/module/tyre_classification', 'user_token=' . $this->session->data['user_token'], 'SSL')
			);
		} else {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('extension/module/tyre_classification', 'user_token=' . $this->session->data['user_token'] . '&module_id=' . $this->request->get['module_id'], 'SSL')
			);
		}
 
		if (!isset($this->request->get['module_id'])) {
			$data['action'] = $this->url->link('extension/module/tyre_classification', 'user_token=' . $this->session->data['user_token'], 'SSL');
		} else {
			$data['action'] = $this->url->link('extension/module/tyre_classification', 'user_token=' . $this->session->data['user_token'] . '&module_id=' . $this->request->get['module_id'], 'SSL');
		}
 
		$data['cancel'] = $this->url->link('extension/module', 'user_token=' . $this->session->data['user_token'], 'SSL');
 
		if (isset($this->request->get['module_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$module_info = $this->model_setting_module->getModule($this->request->get['module_id']);
		}

		if (isset($this->request->post['m_type'])) {
			$data['m_type'] = $this->request->post['m_type'];
		} elseif (!empty($module_info)) {
			$data['m_type'] = $module_info['m_type'];
		} else {
			$data['m_type'] = '';
		}
		
		if (isset($this->request->post['name'])) {
			$data['name'] = $this->request->post['name'];
		} elseif (!empty($module_info)) {
			$data['name'] = $module_info['name'];
		} else {
			$data['name'] = '';
		}
 
		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($module_info)) {
			$data['status'] = $module_info['status'];
		} else {
			$data['status'] = '';
		}

		$data['show_values_table'] = true;
		if(isset($this->request->post['classification'])){
			foreach ($this->request->post['classification'] as $keyType => $type) {
				foreach ($type as $key => $value) {
					$data['classification'][$keyType][$key]=$value;
				}
			}
		}
		elseif(isset($module_info['classification'])){
			foreach ($module_info['classification'] as $keyType => $type) {
				foreach ($type as $key => $value) {
					$data['classification'][$keyType][$key]=$value;
				}
			}
		}
		else{
			$data['classification']['columns']=['','','','','',''];
			$data['classification']['images']=['','','',''];
			$data['classification']['rows']=['','','',''];
			$data['show_values_table'] = false;
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
 
		$this->response->setOutput($this->load->view('extension/module/tyre_classification', $data));
    }

    protected function validate() {
		
		if (!$this->user->hasPermission('modify', 'extension/module/tyre_classification')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
    }
}