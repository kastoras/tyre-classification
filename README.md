# Tyre Classification Opencart Module

Opencart module that displays in tabular form the explanation of KAMA tire coding for large trucks.

## Installation

With the following instructions you can create the file that can be installed from Opencart extensions.

1. Clone the repository.
1. In the root folder create one folder with the name *upload*.
1. Move folders *admin*, *catalog*, *image* in the *upload* folder.
1. Then delete everything except *install.xml* and *upload* folder.
1. Finally, create a .zip archive with the *upload* folder and *install.xml* file, name this archive *tyre-classification.ocmod.zip* 

## Configuration

### Create a new module

1. Go to Extensions -> Modules and find Tyre Classification module
1. Install the module
1. Finally with the Create Button (+) create a new Tyre Classification module

### Add values

#### Basic Values
1. Add the module's name
1. Select one of the 2 layouts. 
    * All Steel Classification Table, Boxed table git green graphics and open/close button
    * All Steel Classification Table Home, full-width table with Blue Graphics
1. Enable/disable button

#### Table Values

**First step, create table headers**
1. First Add Columns name like this *Highway-1*. *Highway* will be the title, *1* will be the sub title
1. Then add header image path *image/tyre_classification/highway.png*
1. All inputs of Header - Columns section must be inserted
1. Then add headers for the rows. The first letter will be displayed in a different color than the rest characters
1. Also all Header - Rows must be inserted
1. Click on the Save button

**Second step, create table values**
Some other tables will be displayed, using the values you have inserted previously 
Now you can add to these inputs the table values like *N F 1 01* or more codes seperated by comma *N F 2 01,N F 2 02* 

Now the new module is ready to insert in any layout.

## Compatibility

Compatible to Opencart 3 and above. 
Tested on Opencart 3.0.3.2

Compatible to PHP 5.6 and above

## Used by 
`https://elastikapetrana.gr`