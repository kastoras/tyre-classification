<?php 
class ControllerExtensionModuleTyreClassification extends Controller {

    public function __construct($params){
        parent::__construct($params);
    }
    
    /**
     * Method index, prints tyre classification table with the data that is stored in db for this module.
     * First gets the data that is stored, and transforms them in correct way to build the table.
     *
     * @param array $setting Settings stored for this module
     *
     * @return twig View file with data
     */
    public function index($setting) {

        $this->document->addStyle('catalog/view/javascript/tyre_classification/styles.css');
        $this->document->addScript('catalog/view/javascript/tyre_classification/script.js');

        $layoutType = $setting['m_type'];
        
        $data['block_full_width']=1;
        $data['images'] = $setting['classification']['images']; 
        
        //Create appropriate format to build table at view (header and footer)
        $data['columns_header'] = $data['columns_footer'] = [];
        foreach($setting['classification']['columns'] as $column ){
            
            //split title & subtitle, it is stored in db with ','
            $columnData = explode(',',$column);
            $data['columns_header'][] = [
                'title'=>$columnData[0],
                'subtitle'=>$columnData[1]
            ];
            $data['columns_footer'][]=$columnData[1];
        }
        
        //Create appropriate format to build table at view (values)
        $data['elements'] = [];
        foreach($setting['classification']['elements'] as $keyCol => $column){            
            foreach($column as $keyRow => $elementValue){
                //split different dimentions in on cell
                $dimensions = explode(',',$elementValue);
                $dimensionBreak = [];
                foreach ($dimensions as $dimension) {
                    $dimensionBreak[] = explode(' ',$dimension);
                }

                //column start from 1, 0 will be given to row title
                $data['elements'][$keyRow][$keyCol+1] = $dimensionBreak;
            }
        }

        //Add row title
        foreach ($setting['classification']['rows'] as $key => $value) {
            array_unshift($data['elements'][$key],[
                'first_letter'=> $value[0],
                'rest_of_letters'=>substr($value, 1)
            ]);
        }

        /**
         * Removes the column if there is no header for it 
         */
        foreach ($data['columns_header'] as $colKey => $value) {
            if($value['title'] == ''){                
                unset($data['images'][$colKey]);
                $data['images'] = array_values($data['images']);
                unset($data['columns_footer'][$colKey]);
                $data['columns_footer'] = array_values($data['columns_footer']);
                unset($data['columns_header'][$colKey]);
                $data['columns_header'] = array_values($data['columns_header']);
                foreach ($data['elements'] as $rowKey => $row) {
                    unset($row[$colKey+1]);                    
                    $data['elements'][$rowKey] = array_values($row);
                }
            }
        }
      
        return $this->load->view('extension/module/tyre_classification/'.$layoutType, $data);
        
    }

}